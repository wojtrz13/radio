sox -S $1 -r ${2:-44100} -b 16 -e signed-integer -c 2 -t raw - | pv -q -L $((${2:-44100}*4))
