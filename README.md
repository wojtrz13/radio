# Internet radio

## Build

You will need boost program options library, you can install it by running:
```
sudo apt install libboost-program-options-dev
```

Building the program:
```
git clone https://bitbucket.org/wojtrz13/radio.git
cd radio
make
```

## Usage

The sender reads an audio stream from stdin, so you can run it like this:
```
sox -S "bonkers.wav" -r 44100 -b 16 -e signed-integer -c 2 -t raw - | pv -q -L $((44100*4)) | ./sikradio-sender -a 239.10.11.12 -n "Radio bonkers"
```
To run the receiver:
```
./sikradio-receiver | play -t raw -c 2 -r 44100 -b 16 -e signed-integer --buffer 32768 -
```
