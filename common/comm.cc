#include <iostream>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cstring>
#include <string>
#include <memory>
#include <cctype>
#include <sstream>
#include <endian.h>

#include "comm.h"
#include "err.h"

using namespace std;
using namespace boost;

const unsigned int MAX_PACKET_SIZE = 65507;

tcp_receiver::tcp_receiver(int sock) : sock(sock), buffer() {
}

string tcp_receiver::recv_command() {
    string ret;
    while (!check_command(ret)) {
        if (buffer.empty()) {
            recv_raw(sock, buffer);
            if (buffer.size() == 0) {
                return string();
            }
        }
        ret += buffer[0];
        buffer.erase(buffer.begin());
    }
    return ret;
}

bool tcp_receiver::check_command(string& str) const {
    while (!check_substr(str)) {
        str.erase(str.begin());
    }
    for (string cmd : {UP, DOWN, UI_EXIT}) {
        if (str == cmd) {
            return true;
        }
    }
    return false;
}

bool tcp_receiver::check_substr(string& str) const {
    if (str.size() == 0) {
        return true;
    }
    for (string cmd : {UP, DOWN, UI_EXIT}) {
        if (str == cmd.substr(0, str.size())) {
            return true;
        }
    }
    return false;
}

namespace {
    struct addrinfo* get_addr(const string& addr, in_port_t port) {
        struct addrinfo addr_hints;
        struct addrinfo *addr_result;

        (void) memset(&addr_hints, 0, sizeof(struct addrinfo));
        addr_hints.ai_family = AF_INET; // IPv4
        addr_hints.ai_socktype = SOCK_DGRAM;
        addr_hints.ai_protocol = IPPROTO_UDP;
        addr_hints.ai_flags = 0;
        addr_hints.ai_addrlen = 0;
        addr_hints.ai_addr = NULL;
        addr_hints.ai_canonname = NULL;
        addr_hints.ai_next = NULL;
        vector<int8_t> adr(addr.begin(), addr.end());
        adr.push_back(static_cast<int8_t>('\0'));
        string port_str = to_string(port);
        vector<int8_t> prt(port_str.begin(), port_str.end());
        prt.push_back(static_cast<int8_t>('\0'));
        int code;
        if (port == 0) {
            code = getaddrinfo(reinterpret_cast<char*>(adr.data()), NULL, &addr_hints, &addr_result);
        } else {
            code = getaddrinfo(reinterpret_cast<char*>(adr.data()), reinterpret_cast<char*>(prt.data()), &addr_hints, &addr_result);
        }
        if (code != 0) err(gai_strerror(code));
        return addr_result;
    }
}

int establish_recv_mcast(const string& addr, in_port_t port) {
    int sock;
    struct sockaddr_in local_address;
    struct ip_mreq ip_mreq;

    struct addrinfo *addr_result = get_addr(addr, 0);

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0) err("socket");

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) err("setsockopt");

    enable = 0;
    if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_ALL, &enable, sizeof(int)) < 0) err("setsockopt");

    ip_mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    ip_mreq.imr_multiaddr.s_addr = ((struct sockaddr_in*) (addr_result->ai_addr))->sin_addr.s_addr;
    if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void*)&ip_mreq, sizeof ip_mreq) < 0) err("setsockopt");

    local_address.sin_family = AF_INET;
    local_address.sin_addr.s_addr = htonl(INADDR_ANY);
    local_address.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *) &local_address, sizeof local_address) < 0) err("bind");
    freeaddrinfo(addr_result);
    return sock;
}

int establish_recv(in_port_t port) {
    int sock;
    struct sockaddr_in local_address;

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0) err("socket");

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) err("setsockopt");

    local_address.sin_family = AF_INET;
    local_address.sin_addr.s_addr = htonl(INADDR_ANY);
    local_address.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *) &local_address, sizeof local_address) < 0) err("bind");
    return sock;
}

int establish() {
    int sock;

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0) err("socket");

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) err("setsockopt");

    return sock;
}

int establish_mbcast() {
    int sock;

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0) err("socket");

    int optval = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void*) &optval, sizeof optval) < 0) err("setsockopt broadcast");

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) err("setsockopt");

    return sock;
}

int establish_send_mbcast(const string& addr, in_port_t port) {
    int sock;
    int optval;

    struct sockaddr remote_address;

    struct addrinfo *addr_result = get_addr(addr, port);

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0) err("socket");

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) err("setsockopt");

    optval = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void*) &optval, sizeof optval) < 0) err("setsockopt broadcast");

    remote_address = *(addr_result->ai_addr);
    if (connect(sock, (struct sockaddr *) &remote_address, sizeof remote_address) < 0) err("connect");
    freeaddrinfo(addr_result);
    return sock;
}

int establish_send(const string& addr, in_port_t port) {
    int sock;

    struct sockaddr remote_address;

    struct addrinfo *addr_result = get_addr(addr, port);

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0) err("socket");

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) err("setsockopt");

    remote_address = *(addr_result->ai_addr);
    if (connect(sock, (struct sockaddr *) &remote_address, sizeof remote_address) < 0) err("connect");
    freeaddrinfo(addr_result);
    return sock;
}

int establish_listen(in_port_t port) {
    int sock;
    struct sockaddr_in server;

    sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0) err("socket");

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) err("setsockopt");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *) &server, sizeof(server)) < 0) err("bind");
    if (listen(sock, 128) < 0) err("listen");
    return sock;
}

void get_sender(const struct sockaddr_in& sa, sender& sender) {
    sender.port = ntohs(sa.sin_port);

    vector<int8_t> tmp(MAX_ADDR);
    if (getnameinfo((struct sockaddr*)&sa, sizeof(sa), reinterpret_cast<char*>(tmp.data()), MAX_ADDR, nullptr, 0, 0) < 0) {
        err("getnameinfo");
    }
    sender.addr = string(tmp.begin(), tmp.end());
    sender.addr.resize(strlen(sender.addr.data()));
}

void send_raw(int sock, const std::string& buffer) {
    vector<int8_t> tmp(buffer.begin(), buffer.end());

    if (send(sock, reinterpret_cast<char*>(tmp.data()), tmp.size(), 0) != static_cast<long>(tmp.size())) err("send");
}

void sendto_raw(int sock, const std::string& buffer, const std::string& addr, in_port_t port) {
    struct addrinfo *addr_result = get_addr(addr, port);
    struct sockaddr remote_address;

    vector<int8_t> tmp(buffer.begin(), buffer.end());

    remote_address = *(addr_result->ai_addr);
    if (sendto(sock, tmp.data(), tmp.size(), 0, &remote_address, sizeof(remote_address)) < 0) err("sendto");
    freeaddrinfo(addr_result);
}

void send_tcp(int sock, const std::string& buffer) {
    vector<int8_t> tmp(buffer.begin(), buffer.end());
    unsigned int sent = 0;
    while (sent < tmp.size()) {
        int piece = send(sock, tmp.data() + sent, tmp.size() - sent, 0);
        if (piece <= 0) err("send");
        sent += piece;
    }
}

void recv_raw(int sock, string& buffer) {
    vector<int8_t> tmp(MAX_PACKET_SIZE);
    int l;
    if ((l = recv(sock, tmp.data(), MAX_PACKET_SIZE, 0)) < 0) err("recv");

    tmp.resize(l);
    buffer = string(tmp.begin(), tmp.end());
}

void recvfrom_raw(int sock, string& buffer, sender& from) {
    vector<int8_t> tmp(MAX_PACKET_SIZE);
    socklen_t fromlen = sizeof(from);
    struct sockaddr_in addr_from;
    addr_from.sin_family = AF_INET;
    int l;
    if ((l = recvfrom(sock, tmp.data(), MAX_PACKET_SIZE, 0, (struct sockaddr*)&addr_from, &fromlen)) < 0) err("recvfrom");

    tmp.resize(l);
    get_sender(addr_from, from);
    buffer = string(tmp.begin(), tmp.end());
}

string stringify(const package& package) {
    vector<int8_t> tmp(16);
    uint64_t session_id = htobe64(package.session_id);
    uint64_t first_byte_num = htobe64(package.first_byte_num);
    memcpy(tmp.data(), &session_id, 8);
    memcpy(tmp.data() + 8, &first_byte_num, 8);
    string ret(tmp.begin(), tmp.end());
    ret += package.audio_data;
    return ret;
}

string stringify(const lookup& lookup) {
    return lookup.header + CTRL_END;
}

string stringify(const reply& reply) {
    return reply.header + CTRL_SEP + reply.MCAST_ADDR + CTRL_SEP
            + to_string(reply.DATA_PORT) + CTRL_SEP + reply.nazwa_stacji + CTRL_END;
}

vector<string> stringify(const rexmit& rexmit) {
    unsigned int i = 0;
    vector<string> ret;
    while (i < rexmit.packages.size()) {
        string buffer = rexmit.header + CTRL_SEP;
        bool full = false;
        while (i < rexmit.packages.size() && !full) {
            if (buffer.size() + (to_string(rexmit.packages[i]) + REX_SEP).size() > MAX_PACKET_SIZE) {
                full = true;
            } else {
                buffer += to_string(rexmit.packages[i]) + REX_SEP;
            }
            ++i;
        }
        buffer[buffer.size() -1] = CTRL_END;
        ret.push_back(buffer);
    }
    return ret;
}

namespace {
    template<typename T>
    T parse_unsigned(const string& input) {
        if (input.size() == 0) {
            throw invalid_format();
        }
        T ret = 0;
        T m1 = numeric_limits<T>::max() / 10;
        T m2 = numeric_limits<T>::max() % 10;
        for (unsigned int i = 0; i < input.size(); ++i) {
            if (!isdigit(input[i]) || ret > m1 || (ret == m1 && static_cast<T>(input[i] - '0') > m2)) {
                throw invalid_format();
            }
            ret = ret * 10 + input[i] - '0';
        }
        return ret;
    }
}

package parse_package(const string& buffer) {
    if (buffer.size() < 17) {
        throw invalid_format();
    }

    vector<int8_t> tmp(buffer.begin(), buffer.begin() + 16);

    package package(0);

    memcpy(&(package.session_id), tmp.data(), 8);
    memcpy(&(package.first_byte_num), tmp.data() + 8, 8);
    package.audio_data = string(buffer.begin() + 16, buffer.end());
    package.session_id = be64toh(package.session_id);
    package.first_byte_num = be64toh(package.first_byte_num);
    if (package.first_byte_num % package.audio_data.size() != 0) {
        throw invalid_format();
    }
    return package;
}

alert parse_alert(const string& buffer) {
    stringstream ss(buffer);
    string header;
    getline(ss, header, CTRL_SEP);

    if (header == lookup::header + CTRL_END) {
        return lookup();
    } else if (header == reply::header) {
        string addr;
        string port_str;
        string name;
        getline(ss, addr, CTRL_SEP);
        getline(ss, port_str, CTRL_SEP);
        in_port_t port = parse_unsigned<in_port_t>(port_str);
        getline(ss, name, CTRL_END);
        if (name.size() > MAX_NAME) {
            throw invalid_format();
        }
        ss.get();
        if (!ss.eof()) {
            throw invalid_format();
        }
        return reply(addr, port, name);
    } else if (header == rexmit::header) {
        vector<uint64_t> packages;
        string pkg_str;
        while (getline(ss, pkg_str, REX_SEP)) {
            if (pkg_str[pkg_str.size() - 1] == CTRL_END) {
                pkg_str.resize(pkg_str.size() - 1);
            }
            uint64_t pkg = parse_unsigned<uint64_t>(pkg_str);
            packages.push_back(pkg);
        }
        return rexmit(packages);
    }

    throw invalid_format();
}
