#include <iostream>
#include <string.h>
#include <errno.h>

#include "err.h"

using namespace std;

#define FATAL_CODE 1

void fatal(const string& error) {
    cerr << error << "\n";
    exit(FATAL_CODE);
}

void errlog(const string& error) {
    cerr << error << ": " << strerror(errno) << "\n";
}

void err(const string& error) {
    cerr << error << ": " << strerror(errno) << "\n";
    throw net_exception();
}
