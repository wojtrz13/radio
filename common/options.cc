#include <boost/program_options.hpp>

#include "options.h"

using namespace std;
using namespace boost;

void validate_size(const string& param, unsigned int size) {
    namespace po = boost::program_options;
    if (param.size() > size) {
        throw po::invalid_option_value(param);
    }
}
