#include <boost/program_options.hpp>

#include "protocol_types.h"

using namespace std;

bool compare_package::operator() (const package& p1, const package& p2) const {
       return p1.first_byte_num < p2.first_byte_num;
}

bool compare_package::operator() (const package& p1, uint64_t num2) const {
       return p1.first_byte_num < num2;
}

bool compare_package::operator() (uint64_t num1, const package& p2) const {
       return num1 < p2.first_byte_num;
}

package::package(unsigned int psize) : session_id(0), first_byte_num(0), audio_data(psize, 0) {
}

const string lookup::header = "ZERO_SEVEN_COME_IN";

const string reply::header = "BOREWICZ_HERE";

const string rexmit::header = "LOUDER_PLEASE";

reply::reply(const string& MCAST_ADDR, unsigned short DATA_PORT, const string& nazwa_stacji) :
    MCAST_ADDR(MCAST_ADDR), DATA_PORT(DATA_PORT), nazwa_stacji(nazwa_stacji) {
}

rexmit::rexmit(const vector<uint64_t>& packages) : packages(packages) {
}
