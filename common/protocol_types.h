#ifndef PROTOCOL_TYPES_H_
#define PROTOCOL_TYPES_H_

#include <vector>
#include <cstdint>
#include <string>
#include <boost/variant.hpp>

struct package {
    uint64_t session_id;
    uint64_t first_byte_num;
    std::string audio_data;

    package(unsigned int psize);
};

class compare_package {
public:
    using is_transparent = void;

    bool operator() (const package& p1, const package& p2) const;
    bool operator() (const package& p1, uint64_t num2) const;
    bool operator() (uint64_t num1, const package& p2) const;
};

struct lookup {
    static const std::string header;
};

struct reply {
    static const std::string header;
    std::string MCAST_ADDR;
    unsigned short DATA_PORT;
    std::string nazwa_stacji;

    reply(const std::string& MCAST_ADDR, unsigned short DATA_PORT, const std::string& nazwa_stacji);
};

struct rexmit {
    static const std::string header;
    std::vector<uint64_t> packages;

    rexmit(const std::vector<uint64_t>& packages);
};

using alert = boost::variant<lookup, reply, rexmit>;

class invalid_format : public std::exception {
};

const unsigned int MAX_NAME = 64;
const unsigned int MAX_ADDR = 64;
const char CTRL_END = '\n';
const char CTRL_SEP = ' ';
const char REX_SEP = ',';

#endif /* PROTOCOL_TYPES_H_ */
