#ifndef _ERR_
#define _ERR_

void fatal(const std::string& error);
void errlog(const std::string& error);
void err(const std::string& error);

class net_exception : public std::exception {
};

#endif
