#ifndef COMM_H_
#define COMM_H_

#include <sys/socket.h>
#include <netinet/in.h>
#include <memory>
#include <vector>
#include "protocol_types.h"

struct sender {
    std::string addr;
    in_port_t port;
};

class tcp_receiver {
private:
    int sock;
    std::string buffer;

public:
    tcp_receiver(int sock);
    std::string recv_command();

private:
    bool check_command(std::string& str) const;
    bool check_substr(std::string& str) const;
};

int establish();
int establish_mbcast();
int establish_recv_mcast(const std::string& addr, in_port_t port);
int establish_send_mbcast(const std::string& addr, in_port_t port);
int establish_recv(in_port_t port);
int establish_send(const std::string& addr, in_port_t port);
int establish_listen(in_port_t port);

void send_raw(int sock, const std::string& buffer);
void sendto_raw(int sock, const std::string& buffer, const std::string& addr, in_port_t port);
void send_tcp(int sock, const std::string& buffer);
void recv_raw(int sock, std::string& buffer);
void recvfrom_raw(int sock, std::string& buffer, sender& from);

std::string stringify(const package& package);
std::string stringify(const lookup& lookup);
std::string stringify(const reply& reply);
std::vector<std::string> stringify(const rexmit& rexmit);
package parse_package(const std::string& buffer);
alert parse_alert(const std::string& buffer);

const std::string SAVE = "\033[?47h";
const std::string SAVE_CURSOR = "\033[s";
const std::string RESTORE = "\033[?47l\033[u";
const std::string CURSOR_TOP_LEFT = "\033[H";
const std::string HIDE_CURSOR = "\033[?25l";
const std::string SHOW_CURSOR = "\033[?25h";
const std::string UP = "\033[A";
const std::string DOWN = "\033[B";
const std::string UI_EXIT = "\04"; // CTRL + D

const char IAC = static_cast<char>(255);
const char WILL = static_cast<char>(251);
const char SGA = static_cast<char>(3);
const char ECHO = static_cast<char>(1);

#endif
