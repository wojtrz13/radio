CC	= g++
CFLAGS	= -Wall -O2 -std=c++17 -IA -IB -Icommon
LFLAGS	= -lboost_program_options -lpthread
VPATH	= A:B:common

all: sikradio-receiver sikradio-sender

sikradio-sender: sikradio-sender.o err.o comm.o protocol_types.o options.o
	$(CC) $(CFLAGS) $^ -o $@ $(LFLAGS)

A: sikradio-sender

sikradio-receiver: sikradio-receiver.o err.o comm.o protocol_types.o options.o
	$(CC) $(CFLAGS) $^ -o $@ $(LFLAGS)

B: sikradio-receiver

sikradio-receiver.o: sikradio-receiver.cc err.h comm.h protocol_types.h options.h
	$(CC) -c $(CFLAGS) $< -o $@ $(LFLAGS)

sikradio-sender.o: sikradio-sender.cc err.h comm.h protocol_types.h options.h
	$(CC) -c $(CFLAGS) $< -o $@ $(LFLAGS)

err.o: err.cc err.h
	$(CC) -c $(CFLAGS) $< -o $@ $(LFLAGS)

comm.o: comm.cc comm.h err.h protocol_types.h
	$(CC) -c $(CFLAGS) $< -o $@ $(LFLAGS)

options.o: options.cc options.h
	$(CC) -c $(CFLAGS) $< -o $@ $(LFLAGS)

protocol_types.o: protocol_types.cc protocol_types.h
	$(CC) -c $(CFLAGS) $< -o $@ $(LFLAGS)

.PHONY: clean all A B
clean:
	rm -f sikradio-sender sikradio-receiver *.o *~ *.bak
