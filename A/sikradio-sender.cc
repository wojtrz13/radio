#include <iostream>
#include <unistd.h>
#include <cstring>
#include <set>
#include <queue>
#include <algorithm>
#include <vector>
#include <chrono>
#include <boost/program_options.hpp>
#include <cstdint>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>

#include "comm.h"
#include "protocol_types.h"
#include "err.h"
#include "options.h"

using namespace std;
using namespace boost;

string MCAST_ADDR;
in_port_t DATA_PORT;
in_port_t CTRL_PORT;
unsigned int PSIZE;
unsigned int FSIZE;
unsigned int RTIME;
string NAZWA;

uint64_t seconds_since_epoch() {
    auto tp = chrono::system_clock::now();
    auto dtn = tp.time_since_epoch();
    return chrono::duration_cast<chrono::seconds>(dtn).count();
}

uint64_t session_id = seconds_since_epoch();
sender from;
mutex fifo_m;
set<package, compare_package> fifo;
mutex rexmits_m;
set<uint64_t> rexmits;
atomic<int> rxm_req_sock;
atomic<int> ctrl_sock;

mutex replies_m;
condition_variable replies_cv;
queue<sender> replies;
atomic<bool> active = true;

void parse_params(int argc, const char * const *argv) {
    namespace po = boost::program_options;

    po::options_description desc("Allowed options");
    desc.add_options()
            (",a", po::value<string>(&MCAST_ADDR)->required()->notifier(bind(validate_size, placeholders::_1, MAX_ADDR)),
                    "adres rozgłaszania kierunkowego")
            (",P", po::value<in_port_t>(&DATA_PORT)->default_value(25540), "port przesyłania danych")
            (",C", po::value<in_port_t>(&CTRL_PORT)->default_value(35540), "port pakietów kontrolnych")
            (",p", po::value<unsigned int>(&PSIZE)->default_value(512), "rozmiar paczki w bajtach")
            (",f", po::value<unsigned int>(&FSIZE)->default_value(131072), "rozmiar kolejki w bajtach")
            (",R", po::value<unsigned int>(&RTIME)->default_value(250), "czas pomiędzy retransmisjami")
            (",n", po::value<string>(&NAZWA)->default_value("Nienazwany Nadajnik")->notifier(bind(validate_size, placeholders::_1, MAX_NAME)),
                    "nazwa nadajnika");
    po::positional_options_description pd;
    po::variables_map vm;
    try {
        po::store(po::command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
        po::notify(vm);
    } catch (po::error &e) {
        fatal(string("Error: ") + e.what());
    }
}

bool read_package(package& package) {
    static uint64_t first_byte_num = 0;

    cin.read(package.audio_data.data(), PSIZE);
    if (cin.gcount() < PSIZE) {
        return false;
    } else {
        package.session_id = session_id;
        package.first_byte_num = first_byte_num;
        first_byte_num += PSIZE;
        return true;
    }
}

void transmit() {
    int data_sock = -1;
    try {
        data_sock = establish_send_mbcast(MCAST_ADDR, DATA_PORT);
    } catch (net_exception& e) {
        fatal("Couldn't initialize data socket");
    }
    package package(PSIZE);
    while (read_package(package)) {
        {
            lock_guard<mutex> lock(fifo_m);
            fifo.insert(package);
            if (fifo.size() * PSIZE > FSIZE) {
                fifo.erase(fifo.begin());
            }
        }
        string buffer = stringify(package);
        try {
            send_raw(data_sock, buffer);
        } catch (net_exception& e) {
            continue;
        }
    }
    close(data_sock);
}

void send_replies() {
    while (active) {
        sender sndr;
        {
            unique_lock<mutex> lock(replies_m);
            if (replies.empty() && active) {
                replies_cv.wait(lock, []{return !replies.empty() || !active;});
            }
            if (!active) {
                break;
            }
            sndr = replies.front();
            replies.pop();
        }
        reply tmp = reply(MCAST_ADDR, DATA_PORT, NAZWA);
        string buffer = stringify(tmp);
        try {
            sendto_raw(rxm_req_sock, buffer, sndr.addr, sndr.port);
        } catch (net_exception& e) {
            cerr << "Couldn't send reply to " << sndr.addr << ":" << sndr.port << "\n";
        }
    }
}

class rxm_req_react : public static_visitor<> {
public:
    void operator()(const lookup& lookup) const {
        (void)lookup;
    }

    void operator()(const reply& reply) const {
        (void)reply;
    }

    void operator()(const rexmit& rexmit) const {
        lock_guard<mutex> lock(rexmits_m);
        for (uint64_t package: rexmit.packages) {
            rexmits.insert(package);
        }
    }
};

class lookup_react : public static_visitor<> {
public:
    void operator()(const lookup& lookup) const {
        (void)lookup;
        lock_guard<mutex> lock(replies_m);
        replies.push(from);
        if (replies.size() == 1) {
            replies_cv.notify_all();
        }
    }

    void operator()(const reply& reply) const {
        (void)reply;
    }

    void operator()(const rexmit& rexmit) const {
        (void)rexmit;
    }
};

void control() {
    while (active) {
        string buffer;
        try {
            recvfrom_raw(ctrl_sock, buffer, from);
        } catch (net_exception& e) {
            cerr << "Couldn't receive control message\n";
            continue;
        }
        if (!active) {
            break;
        }
        alert alert;
        try {
            alert = parse_alert(buffer);
        } catch (invalid_format& e) {
            cerr << "Couldn't parse alert\n";
            continue;
        }
        apply_visitor(lookup_react(), alert);
    }
}

void get_rexmit_request() {
    while (active) {
        string buffer;
        try {
            recv_raw(rxm_req_sock, buffer);
        } catch (net_exception& e) {
            cerr << "Couldn't receive rexmit request\n";
            continue;
        }
        if (!active) {
            break;
        }
        alert alert;
        try {
            alert = parse_alert(buffer);
        } catch (invalid_format& e) {
            cerr << "Couldn't parse alert\n";
            continue;
        }
        apply_visitor(rxm_req_react(), alert);
    }
}

void retransmit() {
    int rexmit_sock = -1;
    try {
        rexmit_sock = establish_send_mbcast(MCAST_ADDR, DATA_PORT);
    } catch (net_exception& e) {
        fatal("Couldn't initialize rexmit data socket");
    }
    chrono::system_clock::duration used_time(0);
    while (active) {
        if (used_time < chrono::milliseconds(RTIME)) {
            this_thread::sleep_for(chrono::milliseconds(RTIME) - used_time);
        }
        chrono::system_clock::time_point start = chrono::system_clock::now();
        set<uint64_t> tmp_rexmits;
        {
            lock_guard<mutex> lock(rexmits_m);
            tmp_rexmits.swap(rexmits);
        }
        for (uint64_t first_byte : tmp_rexmits) {
            unique_lock<mutex> lock(fifo_m);
            auto it = fifo.find(first_byte);
            if (it != fifo.end()) {
                package pkg = *it;
                lock.unlock();
                string buffer = stringify(pkg);
                try {
                    send_raw(rexmit_sock, buffer);
                } catch (net_exception& e) {
                    cerr << "Couldn't send rexmit\n";
                    continue;
                }
            }
        }
        used_time = chrono::system_clock::now() - start;
    }
    close(rexmit_sock);
}

int main(int argc, char **argv) {
    ios::sync_with_stdio(false);
    parse_params(argc, argv);

    try {
        rxm_req_sock = establish();
    } catch (net_exception& e) {
        fatal("Couldn't initialize rexmit request socket");
    }

    try {
        ctrl_sock = establish_recv(CTRL_PORT);
    } catch (net_exception& e) {
        fatal("Couldn't initialize control socket");
    }

    vector<thread> threads;
    threads.push_back(thread(send_replies));
    threads.push_back(thread(get_rexmit_request));
    threads.push_back(thread(control));
    threads.push_back(thread(retransmit));

    transmit();
    {
        lock_guard<mutex> lock(replies_m);
        active = false;
        replies_cv.notify_all();
    }
    shutdown(rxm_req_sock, SHUT_RDWR);
    shutdown(ctrl_sock, SHUT_RDWR);

    for (thread& t : threads) {
        t.join();
    }

    close(ctrl_sock);
    close(rxm_req_sock);
    exit(0);
}
