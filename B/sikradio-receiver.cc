#include <iostream>
#include <vector>
#include <set>
#include <queue>
#include <map>
#include <cstdint>
#include <sys/socket.h>
#include <memory>
#include <boost/program_options.hpp>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <list>
#include <functional>

#include "comm.h"
#include "protocol_types.h"
#include "err.h"
#include "options.h"

using namespace std;
using namespace boost;

string DISCOVER_ADDR;
in_port_t CTRL_PORT;
in_port_t UI_PORT;
unsigned int BSIZE;
unsigned int RTIME;
string NAZWA;

unsigned int LOOKUP_INTERVAL = 5;
unsigned int REPLY_TIMEOUT = 20;

atomic<uint64_t> BYTE0 = 0;
atomic<uint64_t> session_id;
atomic<uint64_t> curr_byte = 0;
atomic<uint64_t> last_byte = 0;
atomic<unsigned int> psize = 0;
mutex data_sock_m;
atomic<int> data_sock;
atomic<int> ctrl_sock;
mutex rexmit_sock_m;
atomic<int> rexmit_sock;
bool prefer_name = false;
atomic<bool> rcv_active = false;
atomic<bool> play_active = false;
condition_variable rcv_cv;
condition_variable play_cv;

atomic<int> play_id = 0;

atomic<int> ui_id = 0;
mutex ui_m;
condition_variable ui_cv;

sender from;

mutex rexmits_m;
condition_variable rexmits_cv;
using rexmit_t = pair<chrono::system_clock::time_point, vector<uint64_t>>;
priority_queue<rexmit_t, vector<rexmit_t>, greater<rexmit_t>> rexmits;

mutex clients_m;
list<int> clients;

mutex buffer_m;
set<package, compare_package> buffer;

struct station {
    string name;
    string addr;
    unsigned short port;
    string ctrl_addr;
    unsigned short ctrl_port;

    station(const string& name, const string& addr, unsigned short port, string ctrl_addr, unsigned short ctrl_port) :
        name(name), addr(addr), port(port), ctrl_addr(ctrl_addr), ctrl_port(ctrl_port) {
    }

    bool operator<(const station& other) const {
        if (name == other.name) {
            if (addr == other.addr) {
                if (port == other.port) {
                    if (ctrl_addr == other.ctrl_addr) {
                        return ctrl_port < other.ctrl_port;
                    }
                    return ctrl_addr < other.ctrl_addr;
                }
                return port < other.port;
            }
            return addr < other.addr;
        }
        return name < other.name;
    }
};

mutex stations_m;
using stations_map_t = map<station, chrono::system_clock::time_point>;
stations_map_t stations;
stations_map_t::iterator curr_station = stations.end();

using station_times_t = set<pair<chrono::system_clock::time_point, station>>;
station_times_t station_times;
condition_variable station_times_cv;

void parse_params(int argc, const char * const *argv) {
    namespace po = boost::program_options;

    po::options_description desc("Allowed options");
    desc.add_options()
            (",d", po::value<string>(&DISCOVER_ADDR)->default_value("255.255.255.255")->notifier(bind(validate_size, placeholders::_1, MAX_ADDR)),
                    "adres wykrywania aktywnych nadajników")
            (",C", po::value<in_port_t>(&CTRL_PORT)->default_value(35540), "port pakietów kontrolnych")
            (",U", po::value<in_port_t>(&UI_PORT)->default_value(15540), "port interfejsu tekstowego")
            (",b", po::value<unsigned int>(&BSIZE)->default_value(65536), "rozmiar bufora w bajtach")
            (",R", po::value<unsigned int>(&RTIME)->default_value(250), "czas pomiędzy raportami")
            (",n", po::value<string>(&NAZWA)->default_value("Nienazwany Nadajnik")->notifier(bind(validate_size, placeholders::_1, MAX_NAME)),
                    "nazwa preferowanego nadajnika");
    po::positional_options_description pd;
    po::variables_map vm;
    try {
        po::store(po::command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
        po::notify(vm);
    } catch (po::error &e) {
        fatal(string("Error: ") + e.what());
    }
    if (!vm["-n"].defaulted()) {
        prefer_name = true;
    }
}

void write_package(const package& package) {
    cout.write(package.audio_data.data(), package.audio_data.size());
}

/* The thread calling this should have buffer_m */
void buffer_insert(const package& package) {
    buffer.insert(package);
    while (!buffer.empty() && last_byte - buffer.begin()->first_byte_num + psize > BSIZE) {
        buffer.erase(buffer.begin());
    }
}

void stop_play() {
    unique_lock<mutex> lock(buffer_m);
    buffer.clear();
    play_active = false;
}

void play() {
    while (true) {
        {
            unique_lock<mutex> lock(buffer_m);
            if (last_byte + psize < BYTE0 + 1 + BSIZE * 3 / 4 || buffer.size() == 0) {
                play_cv.wait(lock, []{return last_byte + psize >= BYTE0 + 1 + BSIZE * 3 / 4 && buffer.size() > 0;});
            }
            play_active = true;
        }
        while (play_active) {
            package pkg(0);
            {
                unique_lock<mutex> lock(buffer_m);
                auto it = buffer.begin();
                if (it == buffer.end()) {
                    play_active = false;
                    buffer.clear();
                    lock.unlock();
                    unique_lock<mutex> lock2(stations_m);
                    rcv_active = false;
                    break;
                }
                pkg = *it;
                buffer.erase(it);
            }
            if (pkg.first_byte_num > curr_byte) {
                stop_play();
                unique_lock<mutex> lock2(stations_m);
                rcv_active = false;
                break;
            }
            write_package(pkg);
            curr_byte += psize;
        }
    }
}

void ask_for_rexmits() {
    while (true) {
        int id = -1;
        chrono::system_clock::time_point next;
        vector<uint64_t> packages;
        {
            unique_lock<mutex> lock(rexmits_m);
            if (rexmits.empty()) {
                rexmits_cv.wait(lock, []{return !rexmits.empty();});
            }
            next = rexmits.top().first;
            packages = rexmits.top().second;
            rexmits.pop();
            id = play_id;
        }
        chrono::system_clock::time_point now = chrono::system_clock::now();
        if (now < next) {
            this_thread::sleep_for(next - now);
        }
        unsigned int i = 0;
        while (i < packages.size()) {
            bool dismiss = false;
            if (last_byte - packages[i] + psize > BSIZE) {
                dismiss = true;
            } else {
                lock_guard<mutex> lock(buffer_m);
                if (buffer.find(packages[i]) != buffer.end()) {
                    dismiss = true;
                }
            }
            if (dismiss) {
                packages.erase(packages.begin() + i);
            } else {
                ++i;
            }
        }
        if (!packages.empty() && id == play_id) {
            rexmit tmp_rexmit = rexmit(packages);
            vector<string> tmp_vec = stringify(tmp_rexmit);
            for (string& tmp : tmp_vec) {
                try {
                    lock_guard<mutex> lock(rexmit_sock_m);
                    send_raw(rexmit_sock, tmp);
                } catch (net_exception& e) {
                    cerr << "Couldn't send rexmit request\n";
                }
            }
            {
                unique_lock<mutex> lock(rexmits_m);
                rexmits.push(make_pair(next + chrono::milliseconds(RTIME), packages));
            }
        }
    }
}

void init_receive(const package& pkg) {
    psize = pkg.audio_data.size();
    session_id = pkg.session_id;
    lock_guard<mutex> lock(buffer_m);
    curr_byte = last_byte = BYTE0 = pkg.first_byte_num;
    buffer_insert(pkg);
}

void receive() {
    while (true) {
        {
            unique_lock<mutex> lock(stations_m);
            if (curr_station == stations.end()) {
                rcv_cv.wait(lock, []{return curr_station != stations.end();});
            }
            rcv_active = true;
        }
        sender station_ctrl;
        {
            lock_guard<mutex> lock(stations_m);
            station_ctrl = {curr_station->first.ctrl_addr, curr_station->first.ctrl_port};
        }
        string tmp;
        try {
            lock_guard<mutex> lock(data_sock_m);
            recv_raw(data_sock, tmp);
        } catch (net_exception& e) {
            continue;
        }
        if (rcv_active) {
            package package(0);
            try {
                package = parse_package(tmp);
            } catch (invalid_format& e) {
                continue;
            }
            if (package.audio_data.size() == 0) {
                continue;
            }
            init_receive(package);
            bool play_activated = false;
            while (rcv_active) {
                try {
                    lock_guard<mutex> lock(data_sock_m);
                    recv_raw(data_sock, tmp);
                } catch (net_exception& e) {
                    continue;
                }
                if (!rcv_active) {
                    break;
                }
                try {
                    package = parse_package(tmp);
                } catch (invalid_format& e) {
                    continue;
                }
                if (package.audio_data.size() != psize || package.first_byte_num % psize != 0 || package.session_id < session_id) {
                    continue;
                }
                if (package.session_id > session_id) {
                    stop_play();
                    play_activated = false;
                    init_receive(package);
                    continue;
                }
                if (package.first_byte_num > last_byte + psize) {
                    vector<uint64_t> skipped;
                    for (uint64_t num = last_byte + psize; num < package.first_byte_num; num += psize) {
                        skipped.push_back(num);
                    }
                    lock_guard<mutex> lock(rexmits_m);
                    rexmits.push(make_pair(chrono::system_clock::now() + chrono::milliseconds(RTIME), skipped));
                    if (rexmits.size() == 1) {
                        rexmits_cv.notify_all();
                    }
                }
                lock_guard<mutex> lock(buffer_m);
                if (package.first_byte_num > last_byte) {
                    last_byte = package.first_byte_num;
                }
                buffer_insert(package);
                if (!play_activated && last_byte + psize >= BYTE0 + 1 + BSIZE * 3 / 4) {
                    play_activated = true;
                    play_cv.notify_all();
                }
            }
        }
    }
}

/* The thread calling this should have stations_m */
void play_station(stations_map_t::iterator it) {
    if (it == stations.end()) {
        return;
    }
    stop_play();
    auto it2 = curr_station;
    curr_station = stations.end();
    rcv_active = false;
    if (it2 != stations.end()) {
        shutdown(data_sock, SHUT_RDWR);
        {
            lock_guard<mutex> lock(data_sock_m);
            close(data_sock);
        }
        {
            lock_guard<mutex> lock(rexmits_m);
            rexmits = priority_queue<rexmit_t, vector<rexmit_t>, greater<rexmit_t>>();
            ++play_id;
        }
        shutdown(rexmit_sock, SHUT_RDWR);
        {
            lock_guard<mutex> lock(rexmit_sock_m);
            close(rexmit_sock);
        }
    }
    data_sock = -1;
    while (data_sock < 0 && !stations.empty()) {
        try {
            data_sock = establish_recv_mcast(it->first.addr, it->first.port);
            rexmit_sock = establish_send(it->first.ctrl_addr, it->first.ctrl_port);
        } catch (net_exception& e) {
            if (data_sock != -1) {
                cerr << "Couldn't establish data socket for station " << it->first.name << "\n";
                shutdown(data_sock, SHUT_RDWR);
                close(data_sock);
                data_sock = -1;
            } else {
                cerr << "Couldn't establish ctrl socket for station " << it->first.name << "\n";
            }
            stations.erase(it++);
            if (it == stations.end()) {
                it = stations.begin();
            }
        }
    }
    curr_station = it;
    rcv_cv.notify_all();
}

/* The thread calling this should have stations_m */
void stop_playing() {
    stop_play();
    auto it = curr_station;
    curr_station = stations.end();
    rcv_active = false;
    if (it != stations.end()) {
        shutdown(data_sock, SHUT_RDWR);
        {
            lock_guard<mutex> lock(data_sock_m);
            close(data_sock);
        }
        {
            lock_guard<mutex> lock(rexmits_m);
            rexmits = priority_queue<rexmit_t, vector<rexmit_t>, greater<rexmit_t>>();
            ++play_id;
        }
        shutdown(rexmit_sock, SHUT_RDWR);
        {
            lock_guard<mutex> lock(rexmit_sock_m);
            close(rexmit_sock);
        }
    }
}

/* The thread calling this should have stations_m and clients_m */
void send_menu(int client_sock) {
    static string header =
            "------------------------------------------------------------------------\r\n"
            "  SIK Radio\r\n"
            "------------------------------------------------------------------------\r\n";
    static string footer = "------------------------------------------------------------------------\r\n";
    send_tcp(client_sock, SAVE);
    send_tcp(client_sock, CURSOR_TOP_LEFT);
    send_tcp(client_sock, header);
    for (auto it = stations.begin(); it != stations.end(); ++it) {
        string prefix;
        if (it == curr_station) {
            prefix = "  > ";
        } else {
            prefix = "    ";
        }
        send_tcp(client_sock, prefix + it->first.name + "\r\n");
    }
    send_tcp(client_sock, footer);
}

void notify_ui() {
    {
        lock_guard<mutex> lock(clients_m);
        if (clients.size() == 0) {
            return;
        }
    }
    lock_guard<mutex> lock(ui_m);
    ++ui_id;
    ui_cv.notify_all();
}

void refresh_ui() {
    int id = ui_id;
    while (true) {
        if (id == ui_id) {
            unique_lock<mutex> lock(ui_m);
            ui_cv.wait(lock, [&]{return ui_id > id;});
        }
        id = ui_id;
        lock_guard<mutex> lock(clients_m);
        lock_guard<mutex> lock2(stations_m);
        for (int client_sock : clients) {
            try {
                send_menu(client_sock);
            } catch (net_exception& e) {
                cerr << "Couldn't send menu\n";
                continue;
            }
        }
    }
}

void erase_stations() {
    while (true) {
        unique_lock<mutex> lock(stations_m);
        if (station_times.empty()) {
            station_times_cv.wait(lock, []{return !station_times.empty();});
        }
        chrono::system_clock::time_point now = chrono::system_clock::now();
        chrono::system_clock::time_point next = station_times.begin()->first;
        if (now < station_times.begin()->first) {
            lock.unlock();
            this_thread::sleep_for(next - now);
        } else {
            auto it = stations.find(station_times.begin()->second);
            if (it == curr_station) {
                auto tmp = it;
                if (stations.size() == 1) {
                    stop_playing();
                } else {
                    ++tmp;
                    if (tmp == stations.end()) {
                        tmp = stations.begin();
                    }
                    play_station(tmp);
                }
            }
            stations.erase(it);
            station_times.erase(station_times.begin());
            lock.unlock();
            notify_ui();
        }
    }
}

class add_station : public static_visitor<> {
public:
    void operator()(const lookup& lkp) const {
        (void)lkp;
    }

    void operator()(const reply& rpl) const {
        {
            station st(rpl.nazwa_stacji, rpl.MCAST_ADDR, rpl.DATA_PORT, from.addr, from.port);
            unique_lock<mutex> lock(stations_m);
            chrono::system_clock::time_point tp = chrono::system_clock::now() + chrono::seconds(REPLY_TIMEOUT);
            auto [it, new_inserted] = stations.insert(make_pair(st, tp));
            if (!new_inserted) {
                station_times.erase(make_pair(it->second, it->first));
            }
            it->second = tp;
            station_times.insert(make_pair(it->second, it->first));
            if (station_times.size() == 1) {
                station_times_cv.notify_all();
            }
            if (new_inserted) {
                if (curr_station == stations.end() || (prefer_name && it->first.name == NAZWA)) {
                    play_station(it);
                }
                lock.unlock();
                notify_ui();
            }
        }
    }

    void operator()(const rexmit& rxm) const {
        (void)rxm;
    }
};

void discover() {
    chrono::system_clock::duration dtn;
    while (true) {
        chrono::system_clock::time_point start = chrono::system_clock::now();
        lookup tmp_lookup = lookup();
        string tmp = stringify(tmp_lookup);
        try {
            sendto_raw(ctrl_sock, tmp, DISCOVER_ADDR, CTRL_PORT);
        } catch (net_exception& e) {
            cerr << "Couldn't send lookup\n";
            continue;
        }
        dtn = chrono::system_clock::now() - start;
        if (dtn < chrono::seconds(LOOKUP_INTERVAL)) {
            this_thread::sleep_for(chrono::seconds(LOOKUP_INTERVAL) - dtn);
        }
    }
}

void get_reply() {
    while (true) {
        string tmp;
        try {
            recvfrom_raw(ctrl_sock, tmp, from);
        } catch (net_exception& e) {
            cerr << "Couldn't receive reply\n";
            continue;
        }
        alert al;
        try {
            al = parse_alert(tmp);
        } catch (invalid_format& e) {
            cerr << "Couldn't parse reply\n";
            continue;
        }
        apply_visitor(add_station(), al);
    }
}

void setup_telnet(int client_sock) {
    string commands = {IAC, WILL, SGA, IAC, WILL, ECHO};
    send_tcp(client_sock, commands);
    send_tcp(client_sock, SAVE_CURSOR);
    send_tcp(client_sock, SAVE);
    send_tcp(client_sock, HIDE_CURSOR);
}

void serve_client(int client_sock) {
    try {
        setup_telnet(client_sock);
    } catch (net_exception& e) {
        cerr << "Couldn't setup telnet\n";
        return;
    }
    list<int>::iterator it;
    send_menu(client_sock);
    {
        lock_guard<mutex> lock(clients_m);
        clients.push_front(client_sock);
        it = clients.begin();
    }
    tcp_receiver rcvr(client_sock);
    string command = "SKIP_FIRST";
    while (command.size() > 0) {
        if (command == UI_EXIT) {
            send_tcp(client_sock, RESTORE);
            send_tcp(client_sock, SHOW_CURSOR);
            break;
        }

        bool next = true;
        bool pass = false;

        if (command == UP) {
            next = false;
        } else if (command == DOWN) {
            next = true;
        } else {
            pass = true;
        }
        if (!pass) {
            unique_lock<mutex> lock(stations_m);
            if (stations.size() > 1) {
                auto station_it = curr_station;
                if (next) {
                    ++station_it;
                    if (station_it == stations.end()) {
                        station_it = stations.begin();
                    }
                } else {
                    if (station_it == stations.begin()) {
                        station_it = stations.end();
                    }
                    --station_it;
                }
                play_station(station_it);
                lock.unlock();
                notify_ui();
            }
        }
        try {
            command = rcvr.recv_command();
        } catch (net_exception& e) {
            cerr << "Couldn't receive command\n";
            continue;
        }
    }
    {
        lock_guard<mutex> lock(clients_m);
        clients.erase(it);
    }
    close(client_sock);
}

void accept_clients() {
    int ui_sock = -1;
    try {
        ui_sock = establish_listen(UI_PORT);
    } catch (net_exception& e) {
        fatal("Couldn't establish ui socket");
    }
    vector<thread> client_threads;
    while (true) {
        int client_sock = accept(ui_sock, NULL, NULL);
        if (client_sock < 0) {
            errlog("accept");
        } else {
            client_threads.push_back(thread([=]{serve_client(client_sock);}));
        }
    }
    for (thread& t : client_threads) {
        t.join();
    }
    close(ui_sock);
}

int main(int argc, char **argv) {
    ios::sync_with_stdio(false);
    parse_params(argc, argv);

    ctrl_sock = establish_mbcast();

    vector<thread> threads;
    threads.push_back(thread(erase_stations));
    threads.push_back(thread(refresh_ui));
    threads.push_back(thread(ask_for_rexmits));
    threads.push_back(thread(play));
    threads.push_back(thread(receive));
    threads.push_back(thread(get_reply));
    threads.push_back(thread(discover));

    accept_clients();

    for (thread& t : threads) {
        t.join();
    }

    /* koniec */
    close(ctrl_sock);
    close(data_sock);
    exit(0);
}
